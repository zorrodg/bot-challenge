process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";


const COOKIE = "connect.sid=s%3A9m5L18wFhmuREJRjikrLHuxnsENioYJV.aVc8%2FMiXSBnWTkjFm3fnGVkk%2F9QUHg35PxkyMXU67s0";
const cheerio = require("cheerio");
const fetch = require("node-fetch");
const qs = require("qs");

const headers = {
  cookie: COOKIE
};

const answers = [
  // Question 1
  function ($question) {
    const numbers = $question.find("span")
      .map(function () {
        return cheerio(this);
      });

    return Number(numbers[0].html()) + Number(numbers[1].html());
  },

  // Question 2
  function ($question) {
    const   numbers = $question.find("span")
      .map(function () {
        return cheerio(this);
      });

    return Number(numbers[0].html()) + Number(numbers[1].html()) + Number(numbers[2].html()) * Number(numbers[3].html());
  },

  // Question 3
  function ($question) {
    var greenSquares = $question.find(".square-matrix b");
    return greenSquares.length;
  }
]

function resolve (step, $question, $form) {
  console.log($question.html())
  const answer = answers[step]($question);
  console.log(answer);

  console.log(qs.stringify({
    ["bot_answer_" + step]: answer,
    submit: "Submit"
  }));

  return fetch("https://10.0.1.44:5555" + $form.attr("action"), {
    method: "POST",
    headers: {
      cookie: COOKIE,
      "Content-Type": "application/x-www-form-urlencoded"
    },
    body: qs.stringify({
      ["bot_answer_" + step]: answer,
      submit: "Submit"
    })
  });
}

function resolveQuestion (step, $) {
  if (step >= resolve.length) {
    console.log("Finished!");
    process.exit();
  }

  return resolve(step, $(".bot-question"), $(".bot-answer > form"))
    .then(res => res.text())
    .then(res => {
      const $ = cheerio.load(res, {
          normalizeWhitespace: true
      });

      if ($(".bot-question")) {
        return resolveQuestion(++step, $);
      }
    });
}

let step = 0;


fetch("https://10.0.1.44:5555/bot-challenge/step/0/go", { headers })
    .then(res => res.text())
    .then(res => {
      let $ = cheerio.load(res, {
          normalizeWhitespace: true
      });
      return resolveQuestion(step, $);
    })
    .catch(err => console.log("Error:", err));
